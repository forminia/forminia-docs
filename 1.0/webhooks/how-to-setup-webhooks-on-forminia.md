# Webhooks

## Setup a webhook

To start using webhooks on Forminia, all you need to do is to setup your endpoint URL to your form. After a successful setup, whenever new data arrives in your form, submission data will be posted to the endpoint URL you provided automatically. Let's make an example with webhook.site:

1. Go to form what you want to set up a webhook and click on "Integrations" on the sidebar.
   ![](./../../assets/img/webhooks/1.png)

2. Enter the webhook endpoint
   ![](./../../assets/img/webhooks/2.png)

3. Don't forget to click on the button to update the form
   ![](./../../assets/img/webhooks/3.png)

4. Now every form submission will send a **POST METHOD** with data to your webhook endpoint
   ![](./../../assets/img/webhooks/4.png)

Extras:

- [https://webhook.site](https://webhook.site/)
- [Wikipedia](https://en.wikipedia.org/wiki/Webhook)
