# Examples

We have a gallery of form examples, [click here to see](https://codepen.io/collection/newQKY).

## Contact Form

Example of simple contact form, [check here](https://codepen.io/paulocastellano/pen/oNYQBGd).

## File Upload

Example of form with file upload, [check here](https://codepen.io/paulocastellano/pen/mdOQWMJ).

## Job Application

Example of job application forrm, [check here](https://codepen.io/paulocastellano/pen/vYyQxrO).
