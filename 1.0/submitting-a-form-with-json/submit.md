# Send form using AJAX/JSON

Changelogfy provides an API to you to interact with your changelogs.

## Submitting forms

```
https://changelogfy.com/api/projects/{api_token}/posts
```

## Submitting a form with file upload

```
https://changelogfy.com/api/projects/{api_token}/posts
```
