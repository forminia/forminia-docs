# Forminia API

Forminia provides API to our customers to collect form submission data.

## Authentication

Forminia require a **Bearer Token in Header** to authenticate in our API.

```js
Authorization: Bearer YOUR-TOKEN-HERE
```

::: warning API Tokens
If you don't have created your api token, please [click here](https://forminia.com/admin/settings/api-tokens) to create your api token.
:::

## Get Form Submissions

Using the form submission API endpoint, you can return a list of submission data using various filtering options that are similar to the functionality available in your Forminia submission dashboard.

**You can get your form endpoint at form settings:**

![Form Endpoint](./../../assets/img/api/form-endpoint.png)

### Sample Request

To fetch all form submission data, here is the example request:

```curl
curl -H 'Accept: application/json' -H "Authorization: Bearer ${YOUR-API-TOKEN}" https://forminia.com/api/forms/{your-form-endpoint}
```

### Sample Respose

```json
{
  "status": "success",
  "data": {
    "current_page": 1,
    "data": [
      {
        "id": 3341414,
        "values": {
          "first_name": "Paulo",
          "last_name": "Castellano",
          "email": "paulo@forminia.com"
        },
        "created_at": "2021-03-07T01:03:32.000000Z"
      },
      {
        "id": 321413,
        "values": {
          "first_name": "Paulo",
          "last_name": "Castellano",
          "email": "paulo@forminia.com"
        },
        "created_at": "2021-03-07T00:42:02.000000Z"
      }
    ],
    "first_page_url": "https://forminia.com/api/forms/{form-endpoint}?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "https://forminia.com/api/forms/{form-endpoint}?page=1",
    "links": [
      {
        "url": null,
        "label": "&laquo; Previous",
        "active": false
      },
      {
        "url": "https://forminia.com/api/forms/{form-endpoint}?page=1",
        "label": "1",
        "active": true
      },
      {
        "url": null,
        "label": "Next &raquo;",
        "active": false
      }
    ],
    "next_page_url": null,
    "path": "https://forminia.com/api/forms/{form-endpoint}",
    "per_page": 20,
    "prev_page_url": null,
    "to": 17,
    "total": 17
  }
}
```

### Filtering Results

While you are requesting your form submission data, there are several filtering options you can use.

#### Page Option

Use the **page** parameter option to gather the results from a specific page.

example:

```curl
curl -H 'Accept: application/json' -H "Authorization: Bearer ${YOUR-API-TOKEN}" https://forminia.com/api/forms/{your-form-endpoint}&page=2
```
