# Introduction

Welcome to Forminia Documentation!

## What is Forminia

Forminia is a forms backend platform that allows you to manage your forms on websites and applications. You can set up a form and start collecting submissions for your form in minutes.

## Requirements

Here's what you need before you start creating your form endpoint:

- The Forminia account
- An HTML form
- A form created in Forminia

```html
<form action="https://forminia.com/f/{form-endpoint}" method="POST">
  <input type="text" name="name" />
  <input type="email" name="email" />
  <input type="text" name="message" />
  <button type="submit">Send</button>
</form>
```

### Html Form

To start using Forminia, you need an HTML form and the sample HTML form looks like this:

```html
<form action="#" method="POST">
  <input type="text" name="name" />
  <input type="email" name="email" />
  <input type="text" name="message" />
  <button type="submit">Send</button>
</form>
```

The **form action="#"** is the most important part of your form because the form endpoint will determine where your data will be stored, will what form.

But to process your form, you usually need to write the form's backend in any programming language like Nodejs, PHP, Java, Python, or Ruby.

But in this case is not necessary, because Forminia will process and store your data!

Forminia will get all values from field contents and sent a POST METHOD to the form endpoint.

All you need to do is get the unique form's endpoint URL for the forms you create, and that unique URL will control the form's backend and allow you to process the forms. The URL of the endpoint you generate in Forminia will look like this:

```curl
https://forminia.com/f/c19b16bc-9bc3-41d4-b372-2902a24aa2f3
```

## Creating a Form

To create a new form and get your unique endpoint URL on Forminia, you first need register and go to the **Forms** page, and then click on the button **Create**. Give a descriptive and unique name and specify a redirect URL for your form.

![](./../assets/img/intro/form-create.gif)

::: danger Important
To set a custom redirect URL, you need to subscribe to any Paid Plan. All Forms on the free plan will be redirected to https://forminia.com/thank-you upon successful submission.
:::
