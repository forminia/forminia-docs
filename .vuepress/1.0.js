module.exports = [
  {
    title: "Getting Started",
    collapsable: false,
    children: ["intro"],
  },
  {
    collapsable: false,
    children: prefix("collecting-submissions", [
      "how-to-collect-form-submission-on-forminia"
    ]),
  },
  {
    collapsable: false,
    children: prefix("submitting-a-form-with-json", ["submit"]),
  },

  {
    collapsable: false,
    children: prefix("webhooks", ["how-to-setup-webhooks-on-forminia"]),
  },
  {
    collapsable: false,
    children: ["faq"],
  },

  {
    collapsable: false,
    children: prefix("api", ["forminia-api"]),
  },

  {
    collapsable: false,
    children: ["example"],
  },
];

function prefix(prefix, children) {
  return children.map((child) => `${prefix}/${child}`);
}
