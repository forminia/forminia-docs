module.exports = {
  title: "Forminia Docs",
  description: "Backend tool for forms.",
  base: "/",
  dest: "public",

  head: [
    [
      "link",
      {
        href:
          "https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i",
        rel: "stylesheet",
        type: "text/css",
      },
    ],
  ],

  themeConfig: {
    displayAllHeaders: true,
    sidebarDepth: 1,

    nav: [
      { text: "Home", link: "https://forminia.com" },
      { text: "Your Accoount", link: "https://forminia.com/login" }
    ],

    sidebar: {
      "/1.0/": require("./1.0"),
    },
  },

  plugins: [
    [
      "@vuepress/google-analytics",
      {
        ga: "G-K3VGWWVNNL",
      },
    ],
  ],
};
